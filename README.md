To create a venv for the project:  
python -m venv venv  
then with bash shell  
source venv/bin/activate  
(venv) will be shown in prompt  
then: pip install flask flask-migrate flask-sqlalchemy flask-login flask_wtf flask-mail pyjwt flask-bootstrap flask-moment flask-babel  
run with: FLASK_APP=microblog.py flask run or shell(for testing)  

The flask packages used:  
flask-sqlalchemy provides ORM (manage dbs wth high-level commands) supporting MySQL, PostgreSQL, SQLite  
flask-login to remember login  
flask-migrate provides "flask db <commands>"  like flask db init, flask db migrate -m "comment"  
the post author is one to many relationship with posts, key of the user at post model is called foreign key as it comes from another model  
flask-mail provides sending of emails (password reset)  
pyjwt for JSON Web Tokens  
flask-bootstrap for bootstrap CSS/JS  
flask-moment for using the Moment.js library  
flask-babel for I18n L10n--

run server:  
export FLASK_DEBUG=1 to activate debug mode  
FLASK_APP=microblog.py FLASK_DEBUG=1 flask run  
test users susan, susan2, password: tester  

debug flask shell:  
FLASK_APP=microblog.py FLASK_DEBUG=1 flask shell  
>>> User.query.all()  
